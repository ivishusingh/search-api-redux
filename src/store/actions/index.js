export const fromApi = data => {
  return {
    type: "FETCH",
    payload: data
  };
};
export const ShowAction = (data, i) => {
  return {
    type: "SHOW",
    payload: { data, i }
  };
};
export const AddItem = (rollNo, name, standard, marks, group) => {
  return {
    type: "ADD_ITEM",
    payload: {
      rollNo,
      name,
      standard,
      marks,
      group
    }
  };
};
