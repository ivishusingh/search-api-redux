import { combineReducers } from "redux";

const initialState = {
  Data: []
};
export const getReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH":
      return {
        ...state,
        Data: action.payload
      };
    case "ADD_ITEM":

      return {
        Data: state.Data.concat(action.payload)
      };
    case "SHOW":
  
      return action.payload;
    default:
      return state;
  }
};

export const rootreducer = combineReducers(getReducer);
