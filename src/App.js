import React, { Component } from "react";
import Navbar from "./components/Navbar";
import "./App.css";
import Home from "./components/Home";
import List from "./components/List";
import Add from "./components/Add";
import User from "./components/User";
import { BrowserRouter, Switch, Route } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Navbar />

        <div className="App">
          <BrowserRouter>
            <Switch>
              <Route exact path="/user" component={User} />
              <Route exact path="/" component={Home} />
              <Route exact path="/list" component={List} />
              <Route exact path="/add" component={Add} />
            </Switch>
          </BrowserRouter>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
