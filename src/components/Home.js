import React from "react";
import { Link } from "react-router-dom";

export default function Home() {
  return (
    <div>
      <div className="card App shadow">
        <div className="card-body">
          <h5 className="card-title">Welcome to Api-fetch app</h5>
          <h6 className="card-subtitle mb-2 text-muted">created by vishal</h6>

          <Link to="/list">
            <button className="btn btn-outline-success m-1">Show List</button>
          </Link>
          <Link to="/add">
            <button className="btn btn-outline-warning m-1">Add Users</button>
          </Link>
        </div>
      </div>
    </div>
  );
}
