import React from "react";

export default function Navbar() {
  return (
    <div>
      <nav className="navbar navbar-dark bg-dark">
        <h3 className="navbar-brand">Api-Fetch</h3>
      </nav>
    </div>
  );
}
