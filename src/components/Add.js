import React, { Component } from "react";
import { Link } from "react-router-dom";
import { AddItem } from "./../store/actions";
import { connect } from "react-redux";

export class Add extends Component {
  state = {
    rollNo: "",
    name: "",
    standard: "",
    group: "",
    marks: ""
  };
  InputHandler = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };
  update = e => {
    e.preventDefault();

    this.props.AddItem(
      this.state.rollNo,
      this.state.name,
      this.state.standard,
      this.state.group,
      this.state.marks
    );
  };

  render() {
    return (
      <div>
        <div style={{ width: "28rem" }} className="card">
          <div className="card-body">
            <form>
              <div className="form-group">
                <label>rollNo</label>
                <input
                  onChange={this.InputHandler}
                  id="rollNo"
                  type="number"
                  className="form-control"
                  aria-describedby="emailHelp"
                  placeholder="Enter roll no"
                  required
                />
              </div>
              <div className="form-group">
                <label>name</label>
                <input
                  onChange={this.InputHandler}
                  id="name"
                  type="text"
                  className="form-control"
                  aria-describedby="emailHelp"
                  placeholder="Enter name"
                  required
                />
              </div>
              <div className="form-group">
                <label>className</label>
                <input
                  onChange={this.InputHandler}
                  id="standard"
                  type="text"
                  className="form-control"
                  placeholder="className"
                  required
                />
              </div>
              <div className="form-group">
                <label>Group</label>
                <input
                  onChange={this.InputHandler}
                  id="group"
                  type="text"
                  className="form-control"
                  placeholder="group"
                  required
                />
              </div>
              <div className="form-group">
                <label>Marks</label>
                <input
                  onChange={this.InputHandler}
                  id="marks"
                  type="number"
                  className="form-control"
                  placeholder="marks"
                  required
                />
              </div>{" "}
              <button onClick={this.update} className="btn btn-primary">
                <Link to="/list"> Submit</Link>
              </button>
              <Link to="/list">
                {" "}
                <button className="btn btn-danger">Cancel</button>
              </Link>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return state;
};
export default connect(
  mapStateToProps,
  { AddItem }
)(Add);
