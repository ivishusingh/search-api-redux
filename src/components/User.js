import React, { Component } from "react";
import { ShowAction } from "./../store/actions";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

export class User extends Component {
  render() {
    return (
      <div>
        <div style={{ width: "35rem" }} className="card shadow">
          <div className="card-body">
            <div className="card-title">
              Name:-
              {this.props.i.name}
            </div>
            Class:-{this.props.i.class}
            <br />
            Roll No:-{this.props.i.rollNo}
            <br />
            Group:-{this.props.i.group}
            <br />
            Marks:-
            {this.props.i.marks.s1 +
              this.props.i.marks.s2 +
              this.props.i.marks.s3}
          </div>
          <Link to="/">
            <button className="btn btn-outline-warning m-2">
              Back to Home
            </button>
          </Link>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return state;
};
export default connect(
  mapStateToProps,
  { ShowAction }
)(User);
