import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fromApi, ShowAction } from "./../store/actions";

export class List extends Component {
  state = {
    List: this.props.Data,
    data: this.props.Data
  };
  show = (i, item) => {
    this.props.ShowAction(i, item);
  };
  search = e => {
    const data = this.state.data.filter(
      item =>
        item.name.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1 ||
        item.rollNo.toString().indexOf(e.target.value.toString()) !== -1 ||
        item.group.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1
    );

    this.setState({ List: data });
  };

  componentDidMount() {
    if (this.props.Data.length === 0) {
      axios
        .get("http://www.mocky.io/v2/5c6ceec53700001d0ffa30c2")
        .then(res => Object.values(res.data))
        .then(res => this.props.fromApi(res))
        .then(res => this.setState({ List: res.payload, data: res.payload }));
    }
  }

  render() {
    return (
      <div>
        <div
          style={{ backgroundColor: "black" }}
          className="jumbotron jumbotron-fluid"
        >
          <div className="container">
            <div className="input-group input-group-lg">
              <input
                onChange={this.search}
                placeholder="search here...."
                type="text"
                className="form-control"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-lg"
              />
            </div>

            <Link to="/add">
              <button className="btn btn-outline-success m-2 ">Add New</button>
            </Link>
          </div>
        </div>
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">roll No</th>
              <th scope="col">Name</th>
              <th scope="col">Average marks</th>
              <th scope="col">Groups</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.state.List.map((item, i) => {
              return (
                <tr key={i}>
                  <td>{item.rollNo}</td>
                  <td>{item.name}</td>
                  <td>{item.marks.s1 + item.marks.s3 + item.marks.s2 / 3}</td>
                  <td>{item.group}</td>
                  <td>
                    <Link to="/user">
                      <button
                        onClick={this.show.bind(this, i, item)}
                        className="btn btn-outline-info"
                      >
                        Show Details
                      </button>
                    </Link>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return state;
};

export default connect(
  mapStateToProps,
  { fromApi, ShowAction }
)(List);
